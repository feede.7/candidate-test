------------------------------------------------------------------------------
-- Company: Satellogic S.A
--
-- File: bandwidth_memory_test.vhdl
-- Description: SPI Core
--
-- AXI-SPI device for new candidates
--
-- Author: Federico De La Cruz Arbizu
--
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity En_bandwidth_memory_test is
    generic (
		C_AXI_DATA_WIDTH 	: integer := 32;
		C_AXI_WORD_WIDTH 	: integer := 128;
		C_AXI_N_TRANSACTIONS: integer := 256;
		RESOLUCION_ESTADISTICA: integer := 10	-- 10 bits de resolución esl espectro de estadística. el canal 1024 sería una espera de 1024 ticks de AXI_CLK
	);
    Port ( 	AXI_CLK 	: in  STD_LOGIC;
			AXI_RESETn 	: in  STD_LOGIC;
    		AXI_Signals : inout  STD_LOGIC; -- Resto de señales AXI
    		AXI_Write	: out STD_LOGIC;	-- Señal para decirle al BUS que escriba WData en WAddress
    		AXI_WData 	: out STD_LOGIC_VECTOR(C_AXI_WORD_WIDTH - 1 downto 0);		-- Data para almacenar en Waddress
    		AXI_WAddress: out STD_LOGIC_VECTOR(C_AXI_DATA_WIDTH - 1 downto 0);		-- Address de la DDR para almacenar WData
           	AXI_Ready 	: in  STD_LOGIC;	-- Flag de señalización del BUS indicando que la transacción se completó y se puede volver a escribir
    		           						-- Este Flag lo pienso como un conjunto entre Ready del BUS, Ready de la DDR, VALID de cada uno de ellos y
    		           						-- una aceptación de transacción del estilo ACK o Handshake que haya entre ambos
           	sMem_addra_Estadistica: in  STD_LOGIC_VECTOR(RESOLUCION_ESTADISTICA - 1 downto 0); 	-- Preparado para barrer la memoria y extraer la estadistica para ser enviada a la PC o al CPU para analizar el espectro
           	sMem_douta_Estadistica: out STD_LOGIC_VECTOR(31 downto 0);							-- Aquí se presenta la cantidad de cuentas acumuladas en el bin que se está pidiendo
           	ResetEstadistica 	: in STD_LOGIC													-- Para limpieza del espectro acumulado
	);	
end En_bandwidth_memory_test;

architecture Arq_speed_memory_test of En_bandwidth_memory_test is

	COMPONENT MEM_Estadistica
      PORT (
        a    : IN  STD_LOGIC_VECTOR(RESOLUCION_ESTADISTICA - 1 DOWNTO 0);
        d    : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
        dpra : IN  STD_LOGIC_VECTOR(RESOLUCION_ESTADISTICA - 1 DOWNTO 0);
        clk  : IN  STD_LOGIC;
        we   : IN  STD_LOGIC;
        spo  : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        dpo  : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
      );
    END COMPONENT;
    
	type   States is  (Wait_Ready,Do_Burst,Wait_Busy);
	signal State 	: States := Wait_Ready;
	
	signal couter_burst 		: integer;
	signal couter_between_burst : integer;
	signal Time_between_burst 	: integer;
	signal Data_To_Write		: STD_LOGIC_VECTOR(C_AXI_WORD_WIDTH - 1 downto 0);
	signal Addr_To_Write		: STD_LOGIC_VECTOR(C_AXI_DATA_WIDTH - 1 downto 0);

	type   States_Estadistica 	is 	(S0, S1, S2, S0_PMT, S1_PMT, S2_PMT, S3, S4, S5, S6, S7, S7_1);
	signal State_Estadistica   	: States_Estadistica := S0;
	
	signal sMem_addrb_Estadistica 	: STD_LOGIC_VECTOR(RESOLUCION_ESTADISTICA - 1 downto 0);
	signal sMem_dinb_Estadistica 	: STD_LOGIC_VECTOR(31 downto 0);
	signal sMem_web_Estadistica		: STD_LOGIC;
	signal sMem_doutb_Estadistica	: STD_LOGIC_VECTOR(31 downto 0);
		
	signal i_MemoIn_Estadistica		: unsigned(RESOLUCION_ESTADISTICA - 1 downto 0);
	signal i_MemoOut_Estadistica	: unsigned(RESOLUCION_ESTADISTICA downto 0);

	signal Estadistica_Valid		: STD_LOGIC;
begin

	process(AXI_RESETn,AXI_CLK)
	begin
		if AXI_RESETn = '0' then
			State 			<= Wait_Ready;
			Data_To_Write	<= (others=>'0');
			Addr_To_Write	<= (others=>'0');
		elsif rising_edge(AXI_CLK) then
			AXI_Write <= '0';
			Estadistica_Valid <= '0';
			case State is
				when Wait_Ready =>
					if AXI_Ready = '1' then
						couter_burst 			<= 1;
						State 					<= Do_Burst;
						couter_between_burst 	<= 0;
						Time_between_burst	 	<= couter_between_burst;
						if couter_between_burst >  C_AXI_N_TRANSACTIONS then
							Estadistica_Valid 		<= '1';
						end if;
					end if;

				when Do_Burst =>
					couter_between_burst <= couter_between_burst + 1;
					
					if couter_burst <= C_AXI_N_TRANSACTIONS then
						couter_burst 	<= couter_burst + 1;
						Data_To_Write	<= STD_LOGIC_VECTOR(unsigned(Data_To_Write) + to_unsigned(1,Data_To_Write'length));
						Addr_To_Write	<= STD_LOGIC_VECTOR(unsigned(Addr_To_Write) + to_unsigned(1,Addr_To_Write'length));
						AXI_WData		<= Data_To_Write;
						AXI_WAddress	<= Addr_To_Write;
						AXI_Write <= '1';
					else
						State 			<= Wait_Busy;
					end if;				

				when Wait_Busy =>
					if AXI_Ready = '0' then
						State <= Wait_Ready;
					end if;
					
				-- Aquí se podría implemtar un ciclo de lectura completo del sector de memoria escrita para validad la información y así cerrar el ciclo
					
				when others =>
					State	<= Wait_Ready;
			end case;
		end if;		
		
	end process;

	-- La idea de realizar un espectro de diferencias temporales entre diferentes Burst nos da la idea de cuánto tiempo se tarda en procesar y almacenar 256 transacciones de 128 bits cada una.
	-- Sabiendo esto se puede deducir el ancho de banda que tenemos disponible para el uso de la memoria
	-- Cada bin del espectro representa un tick de AXI_CLK, y tendrá un offset de 256 que es lo que se tarda en cargar el Burst al AXI
	
	Int_MEM_Estadistica : MEM_Estadistica
	PORT MAP(
		a           => sMem_addrb_Estadistica,
		d           => sMem_dinb_Estadistica,
		dpra        => sMem_addra_Estadistica,
		clk         => AXI_CLK,
		we          => sMem_web_Estadistica,
		spo         => sMem_doutb_Estadistica,
		dpo         => sMem_douta_Estadistica
	);
      
	-- Manejo de Estadistica de tiempos
	Espectro_Estadistica :
	process (AXI_CLK,AXI_RESETn,ResetEstadistica)
	begin
		if AXI_RESETn = '0' or ResetEstadistica = '1' then
			State_Estadistica 		<= S0;
			i_MemoIn_Estadistica	<= to_unsigned(0,i_MemoIn_Estadistica'length);
		elsif rising_edge(AXI_CLK) then
	        sMem_web_Estadistica	<= '0';

			case State_Estadistica is
				-- S0 a S2 inicializan la memoria de Estadística a cero
				when S0 =>
					sMem_dinb_Estadistica	<= STD_LOGIC_VECTOR(to_unsigned(0,sMem_dinb_Estadistica'length));
					sMem_addrb_Estadistica <= STD_LOGIC_VECTOR(i_MemoIN_Estadistica);
					State_Estadistica 	<= S1;

				when S1 =>
					sMem_web_Estadistica	<= '1';
					State_Estadistica 	<= S2;

				when S2 =>
					i_MemoIN_Estadistica	<= i_MemoIN_Estadistica + to_unsigned(1,8);
					if i_MemoIN_Estadistica = to_unsigned(2**i_MemoIN_Estadistica'length-1,i_MemoIN_Estadistica'length) then
						State_Estadistica 	<= S3;
					else
						State_Estadistica 	<= S0;
					end if;

				when S3 =>
					if Estadistica_Valid = '0' then
						State_Estadistica 	<= S4;
					end if;

				when S4 =>
					if Estadistica_Valid = '1' then
						sMem_addrb_Estadistica	<= STD_LOGIC_VECTOR(to_unsigned(Time_between_burst,RESOLUCION_ESTADISTICA));
						State_Estadistica		<= S5;		 --  como dato de canal
					end if;

				-- S5: un ciclo que me aseguro que el dato de cuantas salga de la RAM,
				when S5 =>
					State_Estadistica 		<= S6;

				-- S6: en el dato salido de la memoria realizo el incremento y lo vuelvo a cargar (suma de un "poroto" al bin)
				when S6 =>
					sMem_dinb_Estadistica	<= STD_LOGIC_VECTOR(unsigned(sMem_doutb_Estadistica) + to_unsigned(1,sMem_dinb_Estadistica'length));
					State_Estadistica		<= S7;

				when S7 =>
					sMem_web_Estadistica 	<= '1';
					State_Estadistica		<= S7_1;

				when S7_1 =>
					State_Estadistica		<= S3;

				when others =>
					State_Estadistica		<= S3;

			end case;
		end if;
	end process;
	
end Arq_speed_memory_test;
